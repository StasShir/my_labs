#include <stdio.h>

void init(list_t* l);

typedef struct node_t
{
	int value;
	struct node_t* next;
	struct node_t* prev;

} node_t;

typedef struct
{
	int size;
	node_t* head;
}list_t;

void push_back(list_t* l, int val)
{
	node_t* new = (node_t*)malloc(sizeof(node_t));
	node_t* cur;
	new->value = val;
	new->next = NULL;

	if (l->head == NULL)
	{
		l->head = new;
		l->head->prev = NULL;
	}
	else
	{
		cur = l->head;
		while (cur->next != NULL)
			cur = cur->next;

		cur->next = new;
		new->prev = cur;
	}
	l->size++;
}

void print(list_t* l)
{
	node_t* cur = l->head;
	while (cur != NULL)
	{
		printf("%d ", cur->value);
		cur = cur->next;
	}
	printf("\n");
}

void destroy(list_t* l)
{
	node_t* cur = l->head;
	node_t* c_prev = NULL;

	while (cur != NULL)
	{
		c_prev = cur; 
		cur = cur->next;
		free(c_prev);
	}
	
}

void insert(node_t* cur, int val)
{
	node_t* new = (node_t*)malloc(sizeof(node_t));
	new->value = val;
	new->next = cur->next;
	new->prev = cur;
	cur->next->prev = new;
	cur->next = new;
}

void erase(list_t* l, node_t* cur)
{
	if (cur == l->head)
	{
		l->head = cur->next;

		if (cur->next != NULL)
			cur->next->prev = NULL;

		free(cur);
	}
	else
	{
		cur->prev->next = cur->next;
		if (cur->next != NULL)
			cur->next->prev = cur->prev;

		free(cur);
	}

	l->size--;
}

int get(list_t* l, int i)
{
	node_t* cur = l->head;
	int	count = 0;

	if (i < 0 || i > l->size - 1)
	{
		printf("Error, return 1\n");
		return 1;
	}

	while (count != i)
	{
		cur = cur->next;
		count++;
	}

	return cur->value;

}

void set(list_t* l, int i, int val)
{
	node_t* cur = l->head;
	int count = 0;

	if(i<0 || i>=l->size-1)
		printf("Error, list don't have this index");
	else
	{
		while (count != i)
		{
			cur = cur->next;
			count++;
		}
		cur->value = val;
	}

}

int main()
{
	list_t l;
	int i;

	init(&l);
	for(i = 0; i<11; i++)
		push_back(&l, i);
	print(&l);

	insert(l.head->next->next, 30);

	print(&l);

	erase(&l, l.head);

	print(&l);

	printf("%d\n", get(&l, 7));

	set(&l, 2, 100);

	print(&l);

	destroy(&l);

	return 0;
}

void init(list_t* l)
{
	l->size = 0;
	l->head = NULL;
}