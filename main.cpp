#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "filereader.h"
#define SIZE 100
using namespace std;

char* str_malloc()
{
	char* str = (char*)malloc(SIZE * sizeof(char));
	if (str == NULL)
	{
		cout << "Out of memory..." << endl;
			return NULL;

	}
	return str;
}

int main()
{
	FILE* f = fopen("code_c.txt", "r");
	if (f == NULL)
		cout << "File wasn't open..." << endl;
	else
	{
		cout << "File was open." << endl;

		char* word1 = NULL;

		word1 = str_malloc();
	
		if (word1 == NULL)
		{
			cout << "End, memory out..." << endl;
		}
		else
		{
		
			free(word1);
		}

		int fcl = 0;

		fcl = fclose(f);

		if (fcl == 0)
			cout << "File was closed.";
		else
			cout << "File wasn't closed...";
	}
	return 0;
}